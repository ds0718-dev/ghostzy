package net.ngds.snippet.boot.thymeleaf.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import net.ngds.snippet.boot.thymeleaf.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

	List<User> findByName(String name);

}
